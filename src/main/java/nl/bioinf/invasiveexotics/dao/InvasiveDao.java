package nl.bioinf.invasiveexotics.dao;

import nl.bioinf.invasiveexotics.model.Role;
import nl.bioinf.invasiveexotics.model.User;

import java.util.List;

public interface InvasiveDao{

    /**
     * connection logic should be put here
     * @throws DatabaseException
     */
    void connect() throws DatabaseException;

    /**
     * shutdown logic should be put here
     * @throws DatabaseException
     */
    void disconnect() throws DatabaseException;

    List<String[]> getData() throws DatabaseException;

    /**
     * fetches a user by username and password.
     * @param userName
     * @param userPass
     * @return
     * @throws DatabaseException
     */
    User getUser(String userName, String userPass) throws DatabaseException;

    /**
     * inserts a new User.
     * @param userName
     * @param userPass
     * @param email
     * @param role
     * @throws DatabaseException
     */
    void insertUser(String userName, String userPass, String email, Role role) throws DatabaseException;

    void updateData(String dutchName, String englishName, String status, int objectID) throws DatabaseException;

    void insertData(String dutchName, String scientificName, String status, double rbsX, double rbsY, String date_start) throws DatabaseException;

    void deleteData(int objectID) throws DatabaseException;
}