package nl.bioinf.invasiveexotics.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Fetches the data from the Data class
 */

public class DataFetcher {
    public List<Data> responseToJSON(List<String[]> dbdata) {
        List<Data> data = new ArrayList<>();
        for (String[] fields: dbdata) {
            Data eq = new Data(
                    fields[0],
                    fields[1],
                    fields[2],
                    fields[3],
                    fields[4],
                    fields[5],
                    fields[6]);
            data.add(eq);
        }
        return data;
    }
}
