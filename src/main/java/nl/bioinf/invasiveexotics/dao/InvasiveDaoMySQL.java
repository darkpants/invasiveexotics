package nl.bioinf.invasiveexotics.dao;

import nl.bioinf.invasiveexotics.model.Role;
import nl.bioinf.invasiveexotics.model.TransformWGS84DutchRDNew;
import nl.bioinf.invasiveexotics.model.User;

import java.awt.geom.Point2D;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class contains all the sql statements needed once the server has been started.
 */

public final class InvasiveDaoMySQL implements InvasiveDao {
    private static final String GET_USER = "get_user";
    private static final String INSERT_USER = "insert_user";
    private static final String GET_DATA = "get_data";
    private static final String UPDATE_DATA = "update_data";
    private static final String DELETE_DATA = "delete_data";
    private static final String INSERT_DATA = "insert_data";
    private final String url;
    private final String dbUser;
    private final String dbPassword;
    private Connection connection;
    private Map<String, PreparedStatement> preparedStatements = new HashMap<>();

    /*singleton pattern*/
    private static InvasiveDaoMySQL uniqueInstance;




    /**
     * singleton pattern
     * @param url
     * @param dbUser
     * @param dbPassword
     */



    public InvasiveDaoMySQL(String url, String dbUser, String dbPassword) {
        this.url = url;
        this.dbUser = dbUser;
        this.dbPassword = dbPassword;
    }

    /**
     * singleton pattern
     */
    public static InvasiveDaoMySQL getInstance(String url, String dbUser, String dbPassword) {
        //lazy
        if (uniqueInstance == null) {
            uniqueInstance = new InvasiveDaoMySQL(url, dbUser, dbPassword);
        }
        return uniqueInstance;
    }


    @Override
    public void connect() throws DatabaseException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(url, dbUser, dbPassword);
            prepareStatements();
        } catch (Exception e) {
            e.printStackTrace();
            throw new DatabaseException("Something is wrong with the database, see cause Exception",
                    e.getCause());
        }
    }

    /**
     * prepares prepared statements for reuse
     * @throws SQLException
     */
    private void prepareStatements() throws SQLException {
        String fetchQuery = "SELECT * FROM Users WHERE user_name = ? AND user_password = ?";
        PreparedStatement ps = connection.prepareStatement(fetchQuery);
        this.preparedStatements.put(GET_USER, ps);

        String insertQuery = "INSERT INTO Users (user_name, user_password, user_email, user_role) "
                + " VALUES (?, ?, ?, ?)";
        ps = connection.prepareStatement(insertQuery);
        this.preparedStatements.put(INSERT_USER, ps);

        String fetchData = "SELECT id, rbsX, rbsY, scientificName, dutchName, status, date_start FROM InvasiveExotics";
        ps = connection.prepareStatement(fetchData);
        this.preparedStatements.put(GET_DATA, ps);

        String updateData = "UPDATE InvasiveExotics set dutchName = ?, scientificName = ?, status = ? where ID = ?";
        ps = connection.prepareStatement(updateData);
        this.preparedStatements.put(UPDATE_DATA, ps);

        String deleteData = "DELETE from InvasiveExotics where ID = ?";
        ps = connection.prepareStatement(deleteData);
        this.preparedStatements.put(DELETE_DATA, ps);

        String insertData = "INSERT INTO InvasiveExotics (dutchName, scientificName, status, rbsX, rbsY, date_start) "
                + " VALUES (?, ?, ?, ?, ?, ?)";
        ps = connection.prepareStatement(insertData);
        this.preparedStatements.put(INSERT_DATA, ps);

    }

    @Override
    public List<String[]> getData() throws DatabaseException  {
        List<String[]> data = new ArrayList<>();
        try {
            TransformWGS84DutchRDNew t = new TransformWGS84DutchRDNew();
            PreparedStatement ps = this.preparedStatements.get(GET_DATA);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Double rbsX = Double.valueOf(rs.getString("rbsX"));
                Double rbsY = Double.valueOf(rs.getString("rbsY"));
                String scientificName = rs.getString("scientificName");
                String dutchName = rs.getString("dutchName");
                String status = rs.getString("status");
                String id = rs.getString("ID");
                String date = rs.getString("date_start");

                Point2D point = new Point2D.Double(rbsX, rbsY);
                Point2D wgs = t.toWGS84(point);
                String x = String.valueOf(wgs.getX());
                String y = String.valueOf(wgs.getY());

                data.add(new String[]{x, y, scientificName, dutchName, status, id, date});
            }
            rs.close();
            return data;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DatabaseException("Something is wrong with the database, see cause Exception",
                    e.getCause());

        }
    }

    @Override
    public User getUser(String userName, String userPass) throws DatabaseException  {
        try {
            PreparedStatement ps = this.preparedStatements.get(GET_USER);
            ps.setString(1, userName);
            ps.setString(2, userPass);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String userMail = rs.getString("user_email");
                String userIdStr = rs.getString("user_id");
                String userRoleStr = rs.getString("user_role");
                Role role = Role.valueOf(userRoleStr);
                User user = new User(userName, userMail, userPass, role);
                return user;
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DatabaseException("Something is wrong with the database, see cause Exception",
                    e.getCause());

        }
        return null;
    }

    @Override
    public void insertUser(String userName, String userPass, String email, Role role) throws DatabaseException  {
        try{
            PreparedStatement ps = this.preparedStatements.get(INSERT_USER);
            ps.setString(1, userName);
            ps.setString(2, userPass);
            ps.setString(3, email);
            ps.setString(4, role.toString());
            ps.executeUpdate();
            ps.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new DatabaseException("Something is wrong with the database, see cause Exception",
                    ex.getCause());
        }
    }

    @Override
    public void updateData(String dutchName, String scientificName, String status, int objectID) throws DatabaseException{
        System.out.println("reaches update method");
        try{
            PreparedStatement ps = this.preparedStatements.get(UPDATE_DATA);
            ps.setString(1, dutchName);
            ps.setString(2, scientificName);
            ps.setString(3, status);
            ps.setInt(4, objectID);
            ps.executeUpdate();
//            ps.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new DatabaseException("Data update failed",
                    ex.getCause());
        }

    }

    @Override
    public void insertData(String dutchName, String scientificName, String status, double rbsX, double rbsY, String date_start) throws DatabaseException{
        try{
            PreparedStatement ps = this.preparedStatements.get(INSERT_DATA);
            ps.setString(1, dutchName);
            ps.setString(2, scientificName);
            ps.setString(3, status);
            ps.setDouble(4, rbsX);
            ps.setDouble(5, rbsY);
            ps.setString(6,date_start);
            ps.executeUpdate();
//            ps.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new DatabaseException("Data update failed",
                    ex.getCause());
        }

    }

    @Override
    public void deleteData(int objectID) throws DatabaseException{
        try{
            PreparedStatement ps = this.preparedStatements.get(DELETE_DATA);
            ps.setInt(1, objectID);
            ps.executeUpdate();
//            ps.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new DatabaseException("Data update failed",
                    ex.getCause());
        }

    }

    @Override
    public void disconnect() throws DatabaseException {
        try{
            for( String key : this.preparedStatements.keySet() ){
                this.preparedStatements.get(key).close();
            }
        }catch( Exception e ){
            e.printStackTrace();
        }
        finally{
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
