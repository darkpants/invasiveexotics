package nl.bioinf.invasiveexotics.servlets;

import com.google.gson.Gson;
import nl.bioinf.invasiveexotics.dao.DatabaseException;
import nl.bioinf.invasiveexotics.dao.InvasiveDao;
import nl.bioinf.invasiveexotics.dao.InvasiveDaoFactory;
import nl.bioinf.invasiveexotics.model.DataFetcher;
import nl.bioinf.invasiveexotics.model.TransformWGS84DutchRDNew;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "MapDataServlet", urlPatterns = "/mapdata")
public class MapDataServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        final InvasiveDao dataSource = InvasiveDaoFactory.getDataSource();
        try {
            List<String[]> p = dataSource.getData();
            DataFetcher fetcher = new DataFetcher();
            String json = new Gson().toJson(fetcher.responseToJSON(p));

            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(json);
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
//        Point2D point = new Point2D.Double(120700.723, 487525.501);
//        Point2D wgs = t.toWGS84(point);
//        String x = String.valueOf(wgs.getX());
//        String y = String.valueOf(wgs.getY());
//        String[] p = {x, y};

    }
}
