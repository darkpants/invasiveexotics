package nl.bioinf.invasiveexotics.model;

public enum Role {
    GUEST,
    USER,
    ADMIN;
}