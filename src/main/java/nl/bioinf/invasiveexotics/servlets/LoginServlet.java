package nl.bioinf.invasiveexotics.servlets;

import nl.bioinf.invasiveexotics.dao.DatabaseException;
import nl.bioinf.invasiveexotics.dao.InvasiveDao;
import nl.bioinf.invasiveexotics.dao.InvasiveDaoFactory;
import nl.bioinf.invasiveexotics.model.User;
import nl.bioinf.invasiveexotics.webconfig.WebConfig;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * The first servlet that ir run. If user and password match is legal the user will be
 * forwarded to the home page. It achieves this through a simple form.
 */

@WebServlet(name = "LoginServlet", urlPatterns = "/login")
public class LoginServlet extends HttpServlet {
    private TemplateEngine templateEngine;

    @Override
    public void init() throws ServletException {
        //no super.init() required
        System.out.println("[LoginServlet] Running no-arg init()");
        this.templateEngine = WebConfig.getTemplateEngine();
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config); //required in this implementation
        System.out.println("[LoginServlet] Running init(ServletConfig config)");
    }

    @Override
    public void destroy() {
        super.destroy();
        System.out.println("[LoginServlet] Shutting down servlet service");
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());

        HttpSession session = request.getSession();
        String nextPage;

        if (session.getAttribute("user") == null) {
            User user = authenticate(username, password);
            if (user != null) {
                session.setAttribute("user", new User(user.getName(), user.getEmail(), user.getRole()));
                nextPage = "login";
                response.sendRedirect("/map");
            } else {
                ctx.setVariable("message", "Your password and/or username are incorrect");
                nextPage = "login";
            }
        } else {
            nextPage = "login";
            response.sendRedirect("/map");
        }
        templateEngine.process(nextPage, ctx, response.getWriter());
    }

    private User authenticate(String username, String password) {
        final InvasiveDao dataSource = InvasiveDaoFactory.getDataSource();
        try {
            return dataSource.getUser(username, password);
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
        return null;
    }

    //simple GET requests are immediately forwarded to the login page
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Object logout = request.getParameter("logout");

        if (logout != null) {
            session.removeAttribute("user");
            session.removeAttribute("his");
        }

        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());
        templateEngine.process("login", ctx, response.getWriter());
    }
}