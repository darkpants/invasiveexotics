package nl.bioinf.invasiveexotics.dao;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.sql.SQLException;

/**
 * The main file which will create the database. This class will read the file at the provided location and attempt
 * to extract the needed information from it to add it to a database
 */

public class DBFReader {

    public static void main(String[] args) throws IOException, DatabaseException, SQLException {
        DBFReader dbCreator = new DBFReader();
        dbCreator.readLines();
    }

    /**
     * This method will first read the file provided. After this it will attempt to make a connection, drop the table
     * invasive exotics, and recreate it. Once this is done it will extract the correct data from each line and
     * provide these to the insertData function so that is will be added to the database for later use
     */

    public void readLines() throws IOException, SQLException, DatabaseException {
        RandomAccessFile file = new RandomAccessFile("/homes/rvandepol/Desktop/finaltest.csv", "r");
        DatabaseCreator dbCreator = new DatabaseCreator();
        dbCreator.makeConnection();
        dbCreator.dropData();
        dbCreator.createTable();
        String tmp;
        while((tmp = file.readLine()) != null) {
            if(tmp.startsWith("obs_uri")) continue;
            String[] items = tmp.split(";");
            if(items[9].length() > 6 || items[10].length() > 6) continue;
            String dutchName = items[1];
            String scientificName = items[2];
            String status = items[3];
            String amount = items[5];
            double rbsX = Double.parseDouble(items[9]);
            double rbsY = Double.parseDouble(items[10]);
            String date_start = items[14];
            String date_end = items[15];


            dbCreator.insertData(dutchName, scientificName, status, amount, rbsX, rbsY, date_start, date_end);

        }
        dbCreator.disconnect();
    }
}
