package nl.bioinf.invasiveexotics.servlets;


import nl.bioinf.invasiveexotics.dao.DatabaseException;
import nl.bioinf.invasiveexotics.dao.InvasiveDao;
import nl.bioinf.invasiveexotics.dao.InvasiveDaoFactory;
import nl.bioinf.invasiveexotics.model.Data;
import nl.bioinf.invasiveexotics.model.DataFetcher;
import nl.bioinf.invasiveexotics.webconfig.WebConfig;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

@WebServlet(name = "TableServlet", urlPatterns = "/table")
public class TableServlet extends HttpServlet {
    private TemplateEngine templateEngine;

    @Override
    public void init() throws ServletException {
        //no super.init() required
        System.out.println("[TableServlet] Running no-arg init()");
        this.templateEngine = WebConfig.getTemplateEngine();
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config); //required in this implementation
        System.out.println("[TableServlet] Running init(ServletConfig config)");
    }

    @Override
    public void destroy() {
        super.destroy();
        System.out.println("[TableServlet] Shutting down servlet service");
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Locale locale = request.getLocale();
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                locale);
        templateEngine.process("table", ctx, response.getWriter());
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        Locale locale = request.getLocale();
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                locale);


        final InvasiveDao dataSource = InvasiveDaoFactory.getDataSource();
        try {
            List<String[]> p = dataSource.getData();
            DataFetcher fetcher = new DataFetcher();
            List<Data> data = fetcher.responseToJSON(p);

            ctx.setVariable("tableInfo", data);

        } catch (DatabaseException e) {
            e.printStackTrace();
        }

        templateEngine.process("table", ctx, response.getWriter());
    }
}
