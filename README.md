# Invasive Exotics
This application makes it possible to view Invasive plant species in the region of Groningen in the Netherlands.

# How it works
The map is very simple to understand. It is possible to select any point on the map and edit its' information. Note that it is not possible to modify its' location.


This application views all known invasive exotics in Groningen in a map using a coordinate system. 
These coordinates are available in a database and are in the RD-coordinates format. 
This is a system used by the Dutch government and only usable in the Netherlands.
These coordinates are first fetched from the database and then passed to class that will transform 
these coordinates to regular GPS coordinates. These are then added to an Arcgis map.

The map shows the following information about the Invasive species:
1. Dutch name   
Describes the dutch name.
2. Scientific name   
Describes the scientific name
3. Plant status   
Describes the plant status, either alive or dead.

The following section of the readme is meant for developers, or people who want to modify certain aspects of the application.

=============================================================

# Getting started
The first thing that should be done is creating a database table containing the map data.
DatabaseCreator.java will create the database table for you, but it requires you to change the localhost:
To do this simply edit line 36 of DatabaseCreator. The only thing 
that should be changed here is ``` mysql.bin ```. Change this to match the name of the local host that your database is running on.
In DBFReader.java only change the file location on line 26. Ensure that it is a tab separated csv file and that it contains the following attributes:

Note that most of these are not used in the database table, however the csv must contain them because the lines are split and information is
extracted by index.

1. obs_uri,C,254  
Contains the link to a site where this single point is displayed on a map. Not used in the application.
2. spec_dutch,C,254   
The Dutch name of the invasive species.
3. spec_sci,C,254   
The scientific name of the invasive species.   	
4. status,C,254   
Describes whether the invasive exotic is alive or dead.
5. telmethode,C,254   
Not used in the database 	
6. orig_numb,C,254   
Not used in the database 	
7. numb_min,N,33,31   
Not used in the database 	
8. numb_max,N,33,31   
Not used in the database 	
9. unit,C,254   
Not used in the database 	
10. centerx,N,33,31   
The RB-coordinate x.	
11. centery,N,33,31   
The RB-coordinate y. 	
12. area_m2,N,33,31   
Not used in the database 	
13. loc_type,C,254   
Not used in the database	
14. vervaagd,C,254   
Not used in the database 	
15. date_start,D   
The date the invasive exotic is added to the database. 	
16. date_stop,D   
The date the invasive exotic has been exterminated.
17. date_dkkng,L   
Not used in the database 	
18. stage,C,254   
Not used in the database 	
19. genus,C,254   
Not used in the database 	
20. behavior,C,254   
Not used in the database	
21. biotoop,C,254   
Not used in the database 	
22. deathcause,C,254   
Not used in the database 	
23. location,C,254   
Not used in the database 	
24. substraat,C,254   
Not used in the database 	
25. syntaxon,C,254   
Not used in the database 	
26. opp_dkkng,L   
Not used in the database 	
27. protocol,C,254   
Not used in the database 	
28. detmethode,C,254   
Not used in the database 	
29. databehrdr,C,254   
Not used in the database 	
30. dataeigenr,C,254   
Not used in the database 	
31. kwliteit,C,254   
Not used in the database 	
31. srtgroepen,C,254   
Not used in the database 	
32. wnb_vrl,C,254   
Not used in the database 	
33. wnb_hrl,C,254   
Not used in the database 	
34. wnb_andere,C,254   
Not used in the database 	
35. ffwet1,C,254   
Not used in the database 	
36. ffwet2,C,254   
Not used in the database 	
37. ffwet3,C,254   
Not used in the database 	
38. rodelijst,C,254   
Not used in the database 	
39. zoid,N,19,0   
Not used in the database 	
40. sessionid,C,254   
Not used in the database 

If all this is done correctly the program will run and you will be prompted to enter three attributes:

1. Database name   
The name of the database
2. User name   
Your database username
3. Password   
Your password used to connect to the database

If these three parameters have been filled in correctly it will create a database table called ```InvasiveExotics``` and 
read the file and add its contents to the created table.

In order to log into the webappliction you'll need a Users database table containing login information, to create the Users table in your SQL database
simply copy and paste this code and fill in the correct information in the ```INSERT INTO``` statement (note: If you already have a table called "Users" you should probably change the name of the new table otherwise it will delete your old table):
```sql
DROP TABLE IF EXISTS Users;
CREATE TABLE Users (
    user_id INT NOT NULL auto_increment,
    user_name VARCHAR(100) NOT NULL,
    user_password VARCHAR(100) NOT NULL,
    user_email VARCHAR(255) NOT NULL,
    user_role VARCHAR(100) NOT NULL,
    primary key(user_id));

INSERT INTO Users (user_name, user_password, user_email, user_role)
VALUES ('<USER_NAME>', '<USER_PASSWORD>', '<USER@EMAIL.COM>', '<ADMIN/USER/GUEST>');
```
running the ```INSERT INTO``` statement will create new users in the database table, 
the ```CREATE TABLE``` statement only has to be run once to create the table.
The role parameter has no impact on application usage at this time.

The application can now be run and the graphical points will be added to the map and will be visible once you log in.

When the app is launched it will try to establish a connection to the database to gain access to the map data and user information.
In order for this to succeed you need to have a .my.cnf file present in the home directory.

```bash
[client]
user=<database username>
password=<databse passsword>
host=<host-url>
database<database name>
```

In addition add the following dependencies to your build.gradle:
```bash
    //Servlets
    compile 'javax.servlet:javax.servlet-api:3.1.0'
    //Thymeleaf
    compile 'org.thymeleaf:thymeleaf:3.0.11.RELEASE'
    //JUnit5
    testImplementation 'org.junit.jupiter:junit-jupiter-api:5.1.0'
    testRuntimeOnly 'org.junit.jupiter:junit-jupiter-engine:5.1.0'
    compile group: 'mysql', name: 'mysql-connector-java', version: '5.1.6'
    compile files(<PATH TO "BDutils">)
    compile 'com.google.code.gson:gson:2.8.5'
```

Compile files should contain the path to the files found [here](https://michielnoback.github.io/java_gitbook/downloads/DButils-1.0.2.jar)
This tool is used to load your database credentials.

Furthermore, your web.xml should contain the following context parameter:
```bash
    <context-param>
        <param-name>db-type</param-name>
        <param-value>mysql</param-value>
    </context-param>
```