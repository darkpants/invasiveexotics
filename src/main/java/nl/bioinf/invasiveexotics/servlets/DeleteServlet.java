package nl.bioinf.invasiveexotics.servlets;

import nl.bioinf.invasiveexotics.dao.DatabaseException;
import nl.bioinf.invasiveexotics.dao.InvasiveDao;
import nl.bioinf.invasiveexotics.dao.InvasiveDaoFactory;
import nl.bioinf.invasiveexotics.model.Data;
import nl.bioinf.invasiveexotics.model.DataFetcher;
import nl.bioinf.invasiveexotics.webconfig.WebConfig;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Locale;


/**
 * This servlet is used when data is deleted by the user.
 * It extract the objectId from the selected species in the table and passes this
 * to the the InvasiveDaoFactory and deletes this object using its object id
 * from the database.
 */

@WebServlet(name = "DeleteServlet", urlPatterns = "/delete")
@MultipartConfig
public class DeleteServlet extends HttpServlet {

    @Override
    public void init() throws ServletException {
        //no super.init() required
        System.out.println("[TableServlet] Running no-arg init()");
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config); //required in this implementation
        System.out.println("[TableServlet] Running init(ServletConfig config)");
    }

    @Override
    public void destroy() {
        super.destroy();
        System.out.println("[TableServlet] Shutting down servlet service");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (session.getAttribute("user") == null) {
            response.sendRedirect("/login");
        }

        int objectID = Integer.parseInt(request.getParameter("0"));

        final InvasiveDao dataSource = InvasiveDaoFactory.getDataSource();
        try {
            dataSource.deleteData(objectID);
        } catch (DatabaseException e) {
            e.printStackTrace();
        }

    }
}
