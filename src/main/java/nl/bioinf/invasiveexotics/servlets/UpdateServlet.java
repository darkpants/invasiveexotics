package nl.bioinf.invasiveexotics.servlets;

import nl.bioinf.invasiveexotics.dao.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This servlet is used when data is updated by the user.
 * It will extract the provided dutch name, scientific name, status and ObjectID.
 * All of these parameters are then passed to InvasiveDaoMySQL and the data is edited
 * by selecting on the ObjectID and updating its fields.
 */

@WebServlet(name = "UpdateServlet", urlPatterns = "/update")
@MultipartConfig
public class UpdateServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String dutchName = request.getParameter("dutchName");
        String scientificName = request.getParameter("scientificName");
        String status  = request.getParameter("plantState");
        int objectID = Integer.parseInt(request.getParameter("objectId"));

        final InvasiveDao dataSource = InvasiveDaoFactory.getDataSource();
        try {
            dataSource.updateData(dutchName, scientificName, status, objectID);
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
