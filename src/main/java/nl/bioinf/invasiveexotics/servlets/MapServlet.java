package nl.bioinf.invasiveexotics.servlets;


import nl.bioinf.invasiveexotics.webconfig.WebConfig;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Locale;

/**
 * This servlet runs when the user has logged in. If the username and password combination
 * are accepted they will be automatically be redirected to the homepage, where the ARCGIS map is present.
 */

@WebServlet(name = "MapServlet", urlPatterns = "/map")
public class MapServlet extends HttpServlet {
    private TemplateEngine templateEngine;

    @Override
    public void init() throws ServletException {
        //no super.init() required
        System.out.println("[MapServlet] Running no-arg init()");
        this.templateEngine = WebConfig.getTemplateEngine();
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config); //required in this implementation
        System.out.println("[MapServlet] Running init(ServletConfig config)");
    }

    @Override
    public void destroy() {
        super.destroy();
        System.out.println("[MapServlet] Shutting down servlet service");
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Locale locale = request.getLocale();
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                locale);
        templateEngine.process("map", ctx, response.getWriter());
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        Locale locale = request.getLocale();
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                locale);


        templateEngine.process("map", ctx, response.getWriter());
    }
}
