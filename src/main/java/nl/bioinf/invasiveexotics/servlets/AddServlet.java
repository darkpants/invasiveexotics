package nl.bioinf.invasiveexotics.servlets;

import nl.bioinf.invasiveexotics.dao.DatabaseException;
import nl.bioinf.invasiveexotics.dao.InvasiveDao;
import nl.bioinf.invasiveexotics.dao.InvasiveDaoFactory;
import nl.bioinf.invasiveexotics.model.TransformWGS84DutchRDNew;
import nl.bioinf.invasiveexotics.webconfig.WebConfig;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.util.Date;

@WebServlet(name = "AddServlet", urlPatterns = "/add")
@MultipartConfig
public class AddServlet extends HttpServlet {
    private TemplateEngine templateEngine;
    private double xCord;
    private double yCord;

    @Override
    public void init() throws ServletException {
        //no super.init() required
        System.out.println("[LoginServlet] Running no-arg init()");
        this.templateEngine = WebConfig.getTemplateEngine();
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config); //required in this implementation
        System.out.println("[LoginServlet] Running init(ServletConfig config)");
    }

    @Override
    public void destroy() {
        super.destroy();
        System.out.println("[LoginServlet] Shutting down servlet service");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        TransformWGS84DutchRDNew transformWGS84DutchRDNew = new TransformWGS84DutchRDNew();
        Date currentDate = new Date();
        String month = String.valueOf(currentDate.getMonth() + 1);
        String day = String.valueOf(currentDate.getDate());
        String year = String.valueOf(currentDate.getYear() + 1900);
        String date = day + "-" + month + "-" + year;
        System.out.println("Reaches add servlet");
        System.out.println(date);
        String dutchName = request.getParameter("Dutch_name");
        String scientificName = request.getParameter("Scientific_name");
        String status = request.getParameter("Status");
        String option = request.getParameter("coordsystem");

        double x = Double.parseDouble(request.getParameter("x_cord"));
        double y = Double.parseDouble(request.getParameter("y_cord"));
        Point2D point = new Point2D.Double(x, y);

        final InvasiveDao dataSource = InvasiveDaoFactory.getDataSource();

        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());

        if(option.equals("GPS")) {
            System.out.println("GPS");

            if (transformWGS84DutchRDNew.gpsValidCheck(point)) {
                Point2D wgs = transformWGS84DutchRDNew.fromWGS84(point);
                xCord = wgs.getX();
                yCord = wgs.getY();

                try {
                    dataSource.insertData(dutchName, scientificName, status, xCord, yCord, date);
                    System.out.println("added");
                } catch (DatabaseException e) {
                    e.printStackTrace();
                }
            } else {
                ctx.setVariable("inputError", "The given coordinates are incorrect.");
            }
        }


        else if(option.equals("RBC")) {
            System.out.println("RBC");

            if (transformWGS84DutchRDNew.rdValidCheck(point)) {
                xCord = point.getX();
                yCord = point.getY();

                try {
                    dataSource.insertData(dutchName, scientificName, status, xCord, yCord, date);
                    System.out.println("added");
                } catch (DatabaseException e) {
                    e.printStackTrace();
                }
            } else {
                ctx.setVariable("inputError", "The given coordinates are incorrect.");
            }
        }

        templateEngine.process("addData", ctx, response.getWriter());

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());
        templateEngine.process("addData", ctx, response.getWriter());

    }
}
