package nl.bioinf.invasiveexotics.model;

import java.util.Arrays;


/**
 * This class is used to provide data from the database to the arcgis map.
 */

public class Data {
    private String[] latLon;
    private String scientificName;
    private String dutchName;
    private String plantState;
    private String objectId;
    private String date;

    /**
     * Sets the provided data into the private variables;
     */

    public Data(String latitude, String longitude, String scientificName, String dutchName, String status, String id, String date) {
        setCoordinates(latitude, longitude);
        setScientificName(scientificName);
        setDutchName(dutchName);
        setPlantState(status);
        setObjectId(id);
        setDate(date);
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getPlantState() {
        return plantState;
    }

    private void setPlantState(String status) {
        this.plantState = status;
    }

    public String getDutchName() {
        return dutchName;
    }

    private void setDutchName(String dutchName) {
        this.dutchName = dutchName;
    }

    public String getScientificName() {
        return scientificName;
    }

    private void setScientificName(String scientificName) {
        this.scientificName = scientificName;
    }

    public String[] getLatLon() {
        return latLon;
    }

    private void setCoordinates(String latitude, String longitude) {
        this.latLon = new String[]{latitude, longitude};
    }

    @Override
    public String toString() {
        return "Data{" +
                ", latLon=" + Arrays.toString(latLon) + '}';
    }
}
