package nl.bioinf.invasiveexotics.dao;

import nl.bioinf.invasiveexotics.dao.DatabaseException;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Serves to make a database connection and create the table Invasive Exotics.
 */

public class DatabaseCreator {
    private Connection connection;
    public Map<String, PreparedStatement> preparedStatements = new HashMap<>();
    private static final String DROP_DATA = "drop_data";
    private static final String INSERT_DATA = "insert_data";
    private static final String CREATE_TABLE = "create_table";

    /**
     * Attempts to make a connection to a mysql database located on host mysql.bin.
     * If the user provided correct arguments for the database name, user and password
     * a connection will be made, otherwise it will shut down.
     */


    void makeConnection() throws SQLException {
        Scanner dbConnection = new Scanner(System.in);
        System.out.println("Enter database name: ");
        String dbName = dbConnection.nextLine();
        System.out.println("Enter your username: ");
        String dbUser = dbConnection.nextLine();
        System.out.println("Enter your password: ");
        String dbPass = dbConnection.nextLine();
        String dbUrl = "jdbc:mysql://mysql.bin/" + dbName;
        System.out.println("Getting connection");
        connection = DriverManager.getConnection(dbUrl, dbUser, dbPass);
        prepareStatements();
    }

    /**
     * Contains the mysql statements to create the database, delete the table, and fill it with data.
     */

    private void prepareStatements() throws SQLException {
        String dropQuery = "DROP TABLE IF EXISTS InvasiveExotics";
        PreparedStatement ps = connection.prepareStatement(dropQuery);
        this.preparedStatements.put(DROP_DATA, ps);

        String createQuery = "CREATE TABLE InvasiveExotics (\n" +
                "    ID INT not null auto_increment,\n" +
                "    dutchName varchar(100) not null,\n" +
                "    scientificName varchar(100) not null,\n" +
                "    status varchar(50) not null,\n" +
                "    amount varchar(50),\n" +
                "    rbsX double not null,\n" +
                "    rbsY double not null,\n" +
                "    date_start varchar(30) not null, \n" +
                "    date_end varchar(30),\n " +
                "    primary key(ID));";
        ps = connection.prepareStatement(createQuery);
        this.preparedStatements.put(CREATE_TABLE, ps);

        String insertQuery = "INSERT INTO InvasiveExotics (dutchName, scientificName, status, amount, rbsX, rbsY, date_start, date_end) "
                + " VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        ps = connection.prepareStatement(insertQuery);
        this.preparedStatements.put(INSERT_DATA, ps);
    }

    /**
     * This method will drop the table InvasiveExotics if it is called. This method is always called at the beginning
     * when DBFReader.java is run.
     */

    void dropData() throws DatabaseException {
        try{
            PreparedStatement ps = this.preparedStatements.get(DROP_DATA);
            ps.executeUpdate();
            ps.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new DatabaseException("Something is wrong with the database, see cause Exception",
                    ex.getCause());
        }
    }

    /**
     * This method will create the table InvasiveExotics if it is called. This method is always called second
     * when DBFReader.java is run.
     */


    void createTable() throws DatabaseException {
        try{
            PreparedStatement ps = this.preparedStatements.get(CREATE_TABLE);
            ps.executeUpdate();
            ps.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new DatabaseException("Something is wrong with the database, see cause Exception",
                    ex.getCause());
        }
    }

    /**
     * This method will insert the data into table InvasiveExotics if it is called. This method is always called third
     * when DBFReader.java is run.
     */

    void insertData(String dutchName, String scientificName, String status, String amount, double rbsX, double rbsY, String date_start, String date_end) throws DatabaseException  {
        try{
            PreparedStatement ps = this.preparedStatements.get(INSERT_DATA);
            ps.setString(1, dutchName);
            ps.setString(2, scientificName);
            ps.setString(3, status);
            ps.setString(4, amount);
            ps.setDouble(5, rbsX);
            ps.setDouble(6, rbsY);
            ps.setString(7, date_start);
            ps.setString(8, date_end);
            ps.executeUpdate();
//            ps.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new DatabaseException("Something is wrong with the database, see cause Exception",
                    ex.getCause());
        }

    }

    /**
     * Closes the connection to the database. This method is run last when DVFReader.java is run.
     */

    void disconnect() throws DatabaseException, SQLException {
        System.out.println("Closing connection");
        connection.close();
    }

}
